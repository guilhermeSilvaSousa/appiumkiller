package context;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.HashMap;

public class PortMap {

	private HashMap<String, String> portMap;
	private File file;

	public PortMap() throws IOException {
		this.portMap = new HashMap<String, String>();
		init();
	}
	
	public PortMap(String token) throws IOException {
		this.portMap = new HashMap<String, String>();
		init(token);
	}

	protected void initFile() throws IOException{
		this.file = new File("portsUnavailable.dat");
		if(!file.exists())
			file.createNewFile();
	}
	
	protected void initFile(String token) throws IOException{
		//Precisa ser sobrescrita por UserPortMap
	}
	
	private void init() throws IOException{
		initFile();
		loadPorts();
	}
	
	private void init(String token) throws IOException{
		initFile(token);
		loadPorts();
	}
	
	protected void loadPorts() throws IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(this.file)));
		String contentRead = null;
		while ((contentRead = br.readLine()) != null) {
			if (contentRead.length() > 0) {
				String[] ports = contentRead.split(";");
				for (String port : ports) {
					if (!port.equals(""))
						this.addPort(port);
				}
			}
		}
		br.close();
	}

	public void addPort(String port) {
		if (!portMap.containsKey(port))
			portMap.put(port, port);
	}
	
	public void addPort(Integer port) {
		if (!portMap.containsKey(String.valueOf(port)))
			portMap.put(String.valueOf(port), String.valueOf(port));
	}

	public void removePort(String port) {
		if (portMap.containsKey(port)) {
			portMap.remove(port);
		}
	}
	
	public void removePort(Integer port) {
		if (portMap.containsKey(String.valueOf(port))) {
			portMap.remove(String.valueOf(port));
		}
	}
	
	public boolean isPortAvailable(String port){
		return !this.portMap.containsKey(port);
	}
	
	public boolean isPortAvailable(Integer port){
		return !this.portMap.containsKey(String.valueOf(port));
	}
	
	protected File getFile() {
		return file;
	}

	protected void setFile(File file) {
		this.file = file;
	}

	public void saveChanges() throws IOException{
		this.file.delete();
		this.file.createNewFile();
		BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(this.file)));
		bw.write(this.toString());
		bw.close();
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		int i = 0;
		for(String port: this.portMap.keySet()){
			if(i == 0){
				sb.append(port);
			}else{
				sb.append(";" + port);
			}
			i++;
		}
		return sb.toString();
	}
}
