package context;

import java.io.File;
import java.io.IOException;

public class UserPortMap extends PortMap {

	private String token;
	
	public UserPortMap(String token) throws IOException {
		super(token);
	}
	
	public UserPortMap() throws IOException {
		super();
	}

	@Override
	protected void initFile(String token) throws IOException {
		File file = (new File(ConfigurationFolder.getConfigurationFolder(token), "portInUse.dat"));
		if(!file.exists())
			file.createNewFile();
		setFile(file);
	}
	
	@Override
	protected void initFile() throws IOException {
		File file = (new File(ConfigurationFolder.getConfigurationFolder(), "portInUse.dat"));
		if(!file.exists())
			file.createNewFile();
		setFile(file);
	}
}
