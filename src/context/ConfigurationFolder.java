package context;

import java.io.File;

public class ConfigurationFolder {

	/**
	 * Returns the configuration folder multi-user mode
	 * @param serial
	 * @param token
	 * @return
	 */
	public static File getConfigurationFolder(String token){
		File configurationFolder = new File("config_" + token);
		if(!configurationFolder.exists())
			configurationFolder.mkdir();
		return configurationFolder;
	}
	
	/**
	 * Returns the configuration folder for single-user mode
	 * @param serial
	 * @param token
	 * @return
	 */
	public static File getConfigurationFolder(){
		File configurationFolder = new File(System.getProperty("user.dir"));
		if(!configurationFolder.exists())
			configurationFolder.mkdir();
		return configurationFolder;
	}
}
