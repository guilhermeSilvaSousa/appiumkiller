import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import context.UserPortMap;

public class WindowsConsole extends Console{

	@Override
	public void killAllNodeProcess() throws IOException {
		ProcessBuilder pb = new ProcessBuilder("cmd", "/c", "tasklist");
		Process process = pb.start();
		ArrayList<String> nodeProcessList = processListfilter(process.getInputStream());
		if(isThereARunningNodeProcess(nodeProcessList)){
			System.out.println("Killing All running appium process...");
			for(String pid : nodeProcessList){
				killNodeProcess(Integer.parseInt(pid));
			}
		}else{
			System.out.println("No existing running appium process to kill...");
		}
	}

	@Override
	protected ArrayList<String> processListfilter(InputStream inputStream) {
		ArrayList<String> nodeProcessList = new ArrayList<String>();
		Pattern processNameAndPID = Pattern.compile(".*node\\.exe\\s{1,}(\\d{3,5}).*");
		Matcher matcher = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;
		try {
			while((line = br.readLine()) != null){
				matcher = processNameAndPID.matcher(line);
				if(matcher.matches()){
					nodeProcessList.add(matcher.group(1));
				}
			}
			return nodeProcessList;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	protected void killNodeProcess(int pid) throws IOException {
		ProcessBuilder pb = new ProcessBuilder("cmd", "/c", "taskkill /f /pid " + pid);
		Process process = pb.start();
	}

	@Override
	protected Integer getProcessPortByPID(String pid) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void killAllNodeProcess(UserPortMap portsInUse) throws IOException, RuntimeException, InterruptedException {
		// TODO Auto-generated method stub
		
	}
}
