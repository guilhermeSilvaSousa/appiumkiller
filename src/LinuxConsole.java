import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import context.PortMap;
import context.UserPortMap;
import sun.security.action.GetLongAction;

public class LinuxConsole extends Console {

	private PortMap portMap;
	private static Logger LOG;

	static {
		LOG = Logger.getLogger(LinuxConsole.class.getName());
	}
	
	public LinuxConsole() throws IOException{
		this.portMap = new PortMap();	
	}

	@Override
	public void killAllNodeProcess() throws IOException, RuntimeException, InterruptedException {
		ProcessBuilder pb = new ProcessBuilder("sh", "-c", "ps -ef | grep appium");
		Process process = pb.start();
		UserPortMap userPortMap = new UserPortMap();
		ArrayList<String> nodeProcessList = processListfilter(process.getInputStream());
		if (isThereARunningNodeProcess(nodeProcessList)) {
			Integer appiumPort = null;
			for (String pid : nodeProcessList) {
				appiumPort = getProcessPortByPID(pid);
				killNodeProcess(Integer.parseInt(pid));
				this.portMap.removePort(appiumPort);
				userPortMap.removePort(appiumPort);
				LOG.info("[APPIUM KILLER] Appium process listening on " + appiumPort + " was killed");
			}
			this.portMap.saveChanges();
			userPortMap.saveChanges();
			LOG.info("[APPIUM KILLER] Done");
		} else {
			LOG.info("[APPIUM KILLER] No existing running appium process to kill...");
		}
	}

	@Override
	public void killAllNodeProcess(UserPortMap userPortMap)
			throws IOException, RuntimeException, InterruptedException {
		String[] portsInUseByUser = userPortMap.toString().split(";");
		String[] portsUnavailable = portMap.toString().split(";");
		if (portsInUseByUser.length > 0) {
			ProcessBuilder pb = new ProcessBuilder("sh", "-c", "ps -ef | grep appium");
			Process process = pb.start();
			ArrayList<String> portList = new ArrayList<String>();
			for (String port : portsInUseByUser) {
				portList.add(port);
			}
			ArrayList<String> nodeProcessList = processListfilter(process.getInputStream());
			if (isThereARunningNodeProcess(nodeProcessList)) {
				Integer appiumPort = null;
				for (String pid : nodeProcessList) {
					appiumPort = getProcessPortByPID(pid);
					for (String port : portList) {
						if (appiumPort == Integer.parseInt(port)) {
							killNodeProcess(Integer.parseInt(pid));
							LOG.info("[APPIUM KILLER] Appium process listening on " + appiumPort + " was killed");
							userPortMap.removePort(port);
							this.portMap.removePort(port);
						}
					}
				}
				this.portMap.saveChanges();
				userPortMap.saveChanges();
				LOG.info("[APPIUM KILLER] Done");
			} else {
				LOG.info("[APPIUM KILLER] No existing running appium process to kill...");
			}
		} else {
			LOG.warning("[APPIUM KILLER] No appium ports was received");
		}
	}

	@Override
	protected ArrayList<String> processListfilter(InputStream inputStream) {
		String pidRegex = "^";

		ArrayList<String> nodeProcessList = new ArrayList<String>();
		Pattern processNameAndPID = null;
		Matcher matcher = null;
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		try {
			pidRegex = "^\\S{1,}\\s{1,}(\\d{1,}).*node.*appium.*";
			processNameAndPID = Pattern.compile(pidRegex);
			String line = null;
			while ((line = br.readLine()) != null) {
				matcher = processNameAndPID.matcher(line);
				if (matcher.matches()) {
					nodeProcessList.add(matcher.group(1));
				}
			}
			return nodeProcessList;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nodeProcessList;
	}

	private int getPidIndexColumn(String line) {
		int indexColumn = 0;
		if (line.contains("PID")) {
			String[] columns = line.split("\\s{1,}");
			for (; indexColumn < columns.length; indexColumn++) {
				if (columns[indexColumn].equals("PID"))
					return indexColumn;
			}
		}
		return -1;
	}

	protected Integer getProcessPortByPID(String pid) throws RuntimeException, InterruptedException {
		File getProcessPort = new File("get_services_port.sh");
		if (getProcessPort.exists()) {
			ProcessBuilder pb = new ProcessBuilder("sh", getProcessPort.getAbsolutePath());
			try {
				Process process = pb.start();
				process.waitFor();
				if (process.exitValue() != 0)
					throw new RuntimeException("[APPIUM KILLER] Error in get_services_port.sh execution");
				String line = null;
				InputStream inputStream = process.getInputStream();
				InputStream errorStream = process.getErrorStream();
				BufferedReader brInputStream = new BufferedReader(new InputStreamReader(inputStream));
				BufferedReader brErrorStream = new BufferedReader(new InputStreamReader(errorStream));
				Matcher matcher = null;
				Pattern processPort = Pattern
						.compile("^tcp\\s{1,}\\w{1,}\\s{1,}\\d{1,}\\s{1,}\\d{1,}\\s{1,}.*:(\\d{1,}).*\\,p?i?d?=?" + pid
								+ "\\,.*");
				while ((line = brInputStream.readLine()) != null) {
					if ((matcher = processPort.matcher(line)).matches()) {
						return Integer.parseInt(matcher.group(1));
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			return null;
		} else {
			throw new RuntimeException("[APPIUM KILLER] 'get_services_port.sh' script file is required on '"
					+ System.getProperty("user.dir") + "'");
		}
	}

	public static void main(String[] args) throws Exception {
		// System.out.println(new LinuxConsole().getProcessPortByPID("7226"));
	}

	@Override
	protected void killNodeProcess(int pid) throws IOException {
		ProcessBuilder pb = new ProcessBuilder("sh", "-c", "kill -9 " + pid);
		Process process = pb.start();
	}
}
