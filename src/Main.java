import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import context.UserPortMap;

public class Main {

	public static void main(String[] args) throws IOException, RuntimeException, InterruptedException {
		Pattern pattern = Pattern.compile("^token=(.*)$");
		Matcher matcher = null;

		if (args.length == 1 && (matcher = pattern.matcher(args[0])).matches()) {
			Console.getConsoleEnvironment().killAllNodeProcess(new UserPortMap(matcher.group(1)));
		} else if(args.length == 1){
			System.err.println("Incorrect Params received");
		}else{
			Console.getConsoleEnvironment().killAllNodeProcess();
		}
	}
}
