import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import context.UserPortMap;

public abstract class Console {

	public abstract void killAllNodeProcess() throws IOException, RuntimeException, InterruptedException;
	public abstract void killAllNodeProcess(UserPortMap portsInUse) throws IOException, RuntimeException, InterruptedException;
	
	public static Console getConsoleEnvironment() throws IOException{
		String osName = System.getProperty("os.name");
		if(osName.toLowerCase().contains("windows")){
			return new WindowsConsole();
		}else if(osName.toLowerCase().contains("linux")){
			return new LinuxConsole();
		}
		return null;
	}
	
	protected void showProcessOutput(InputStream inputStream){
		BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
		String line = null;
		try {
			while((line = br.readLine()) != null){
				System.out.println(line);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected boolean isThereARunningNodeProcess(ArrayList<String> nodeProcessList){
		return (nodeProcessList.size() > 0)?true:false;
	}
	protected abstract ArrayList<String> processListfilter(InputStream inputStream);
	
	protected abstract void killNodeProcess(int pid) throws IOException;
	
	protected abstract Integer getProcessPortByPID(String pid) throws Exception;
}
